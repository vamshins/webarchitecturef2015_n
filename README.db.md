Install PostgreSQL. (See [official documentation](http://www.postgresql.org/))

Run posgresql server.

    $ postgrsql -D /usr/local/pgsql/data
    
Install postgresql gem.

    $ cd PROJECT_PATH
    $ bundle install
    
Create databse.

    $ rake db:create
    $ rake db:migrate

