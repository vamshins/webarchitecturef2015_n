class CreateChallenges < ActiveRecord::Migration
  def change
    create_table :challenges do |t|
      t.string :name, null: false
      t.string :description, null: false
      t.boolean :moderation_flag, default: false
      t.string :status
      t.text :terms
      t.references :creator, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
